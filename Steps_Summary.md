# Mnist_K_N_means
## Summary of Steps to implement K-Nearest Neighbour:

1) Loaded the digits dataset from sklearn.datasets
2) Used the KNeighborsClassifier of sklearn.neighbors to build the model with n =3
3) Split the data into train and test set in the ratio 80% of original data is used to train the model and 20% data is used to test the predicting capabilities of the model
4) Fitted the test data in the model.
5) Plotted Confusion matrix to find the deviations in prediction, an accuracy of 99.44% was acheived.



## Improving the accuracy:

1) We can incorporate Neural Networks then we can easily improve the accuracy by using a hidden layer neural network with one hot encoding and sigmoid activation at input,
 Relu activation function at the hidden layer,and an output layer of 10 neurons with softmax activation function,since the data are images in black and white binary cross entropy
 loss function is better suitable.

2) In order to improve the accuracy we could normalize the data by dividing it by 255.0 so it comes in the range of 0 to 1.In K nearest Neighbour
to determine closeness, usually the euclidean distance is used. It makes sense to normalize the data before starting the algorithm, 
since euclidean distance would otherwise just be the absolute of the dominant feature

3) SVM can give better accuracy than K-Nearest Neighbour as the later method has high tendency to overfit.



